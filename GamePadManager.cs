﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Asteroids
{
    public class GamePadManager
    {
        private GamePadState _oldGamePadState;
        private GamePadState _currentGamePadState;
        private Texture2D bulletImage;

        public GamePadManager(ContentManager Content)
        {
            bulletImage = Content.Load<Texture2D>("laserRed");
        }

        public void Update(GamePadState state, GraphicsDeviceManager graphics, Sprite playerSprite, BulletList bullets)
        {
            _currentGamePadState = state;



            if (_currentGamePadState.DPad.Left == ButtonState.Pressed)
            {
                playerSprite.Rotation -= 0.05f;
            }
            if (_currentGamePadState.DPad.Right == ButtonState.Pressed)
            {
                playerSprite.Rotation += 0.05f;
            }
            if (WasJustPressed(Buttons.RightShoulder))
            {
                Sprite newBullet = new Sprite(graphics, bulletImage);

                newBullet.velocity = new Vector2((float)Math.Cos(playerSprite.Rotation - MathHelper.PiOver2),
                                                 (float)Math.Sin(playerSprite.Rotation - MathHelper.PiOver2)) * 4.0f
                                                 + playerSprite.velocity;

                newBullet.position = playerSprite.position + newBullet.velocity * 1.75f;
                newBullet.Rotation = playerSprite.Rotation;

                bullets.Add(newBullet);
            }

            if (_currentGamePadState.Buttons.LeftShoulder == ButtonState.Pressed)
            {
                playerSprite.velocity = new Vector2((float)Math.Cos(playerSprite.Rotation - MathHelper.PiOver2),
                                             (float)Math.Sin(playerSprite.Rotation - MathHelper.PiOver2)) / 4.0f
                                             + playerSprite.velocity;
            }
            //if (_currentKeyboard.IsKeyDown(Keys.Down))
            //{
            //    _movement += Vector2.UnitY * .5f;
            //}


            _oldGamePadState = _currentGamePadState;
        }

        bool WasJustPressed(Buttons b)
        {
            return _oldGamePadState.IsButtonUp(b) && _currentGamePadState.IsButtonDown(b);
        }
    }
}
